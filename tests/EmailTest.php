<?php

/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 11/05/2016
 * Time: 10:11
 */

use App\Jobs\SendEmail;
use App\ParsesText;
use App\Message;
use App\Template;
use Illuminate\Support\Collection;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;

class EmailTest extends TestCase
{
        use ParsesText, ProvidesConvenienceMethods;

        private $templates;
        private $defined_template;
        private $request;

        public function setUp(): void
        {
                parent::setUp();
                $this->templates = new \App\Template();
                $this->defined_template = [
                    'title'   => 'Test Template',
                    'type'    => 1,
                    'content' => 'Hello World<br />$param;, $param1;',
                    'key' => \App\Application::first()->key
                ];

                $this->request = [
                        'delimiter' => ';',
                        'start_delimiter' => '$',
                        'end_delimiter' => ';',
                        'parameters' => 'param=1234;param1=5678'
                ];
        }

        public function testSendMail()
        {
                $result = $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $response = json_decode($result->response->getContent());
                $template = Template::where('reference', $response->results->reference)->first();

                $mail = new Message();
                $mail->to = 'phpunit@example.com';
                $mail->from = serialize(["email" => config('app.mail'), "name" => config('app.name')]);
                $mail->template_id = $template->id;

                if (!empty($this->request['parameters'])) {

                        $parameters = $this->explodeEscaped($this->request['delimiter'], $this->request['parameters'], config('app.escape_char'), $this->request['start_delimiter'],null);
                        $newParams = new Collection;
                        foreach ($parameters as $parameter) {
                                list($k, $v) = explode('=', $parameter, 2);
                                $newParams->put($k, $v);
                        }
                        $mail->message = $this->parseText($newParams->toArray(), $template->content, $this->request['end_delimiter']);
                } else {
                        $mail->message = $template->content;
                }

                $mail->subject = $template->title;

                $mail->save();

                $job = (new SendEmail($mail));
                $this->dispatch($job);

                $mail = $mail->fresh();
                $this->assertEquals(Message::STATUS_SENT, $mail->status);
        }
}