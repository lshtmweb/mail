<?php

class RoutesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testHomeInfo()
    {
            $get = $this->get('/');
            $response = json_decode($get->response->getContent());
            $this->assertEquals($response->results->{0}, 'London School of Hygiene and Tropical Medicine - Mail API');
    }
}
