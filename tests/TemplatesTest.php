<?php
use Laravel\Lumen\Testing\DatabaseTransactions;
class TemplatesTest extends TestCase
{

        private $templates;
        private $defined_template;
        private $invalid_template;
        private $update_template;

        public function setUp(): void
        {
                parent::setUp();
                $this->templates = new \App\Template();

                $this->defined_template = [
                    'title'   => 'Test Template',
                    'type'    => 1,
                    'content' => 'Hello World',
                    'key' => \App\Application::first()->key
                ];
                $this->update_template = [
                    'title'   => 'Updated Template',
                    'type'    => 1,
                    'content' => 'Hello World',
                    'key' => \App\Application::first()->key
                ];
                $this->invalid_template = [
                    'title' => 'Test Template',
                    'type'  => 1,
                ];
        }

        /**
         * A basic test example.
         *
         * @return void
         */
        public function testGetTemplates()
        {
                $get = $this->get('/templates');
                $this->assertEquals(200, $get->response->getStatusCode());

        }

        /**
         * Create a template and check for the title in the result.
         */
        public function testCreateTemplate()
        {
                $result = $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $response = json_decode($result->response->getContent());
                $this->seeStatusCode(\Illuminate\Http\Response::HTTP_OK);
                $this->assertEquals($this->defined_template['title'], $response->results->title);
        }

        public function testCreateTemplateValidation()
        {
                $this->post('/templates', $this->invalid_template, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->seeStatusCode(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        public function testUpdateTemplate()
        {
                $templateRequest = $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $template = json_decode($templateRequest->response->content());
                $result = $this->put("/templates/" . $template->results->reference, $this->update_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $response = json_decode($result->response->getContent());
                $this->seeStatusCode(\Illuminate\Http\Response::HTTP_OK);
                $this->assertEquals($this->update_template['title'], $response->results[0]->title);
        }

        public function testDeleteTemplate()
        {
                $templateRequest = $this->post('/templates', $this->defined_template, ['X-Requested-With' => 'XMLHttpRequest']);
                $template = json_decode($templateRequest->response->content());
                $this->delete("/templates/" . $template->results->reference, $this->update_template, ['X-Requested-With' => 'XMLHttpRequest'])
                    ->seeStatusCode(\Illuminate\Http\Response::HTTP_OK);
        }
}
