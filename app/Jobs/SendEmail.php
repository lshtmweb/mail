<?php

namespace App\Jobs;

use App\Attachment;
use App\BaseTemplate;
use App\Message;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class SendEmail extends Job implements ShouldQueue
{
        use InteractsWithQueue, SerializesModels;

        protected $message;

        /**
         * SendEmail constructor.
         *
         * @param Message $message
         */
        public function __construct(Message $message)
        {
                $this->message = $message;
        }

        /**
         * Execute the job
         *
         * @param Mailer $mailer
         */
        public function handle(Mailer $mailer)
        {
                Log::debug(trans('app.messages.email_attempt'));
                try {
                        //If the template has base template parameter set, use it, else use the default.
                        $this->message->template->uses->slug ? $baseTemplate = $this->message->template->uses->slug : $baseTemplate = BaseTemplate::DEFAULT_SLUG;
                        /** @var \Intervention\Image\Image $logo */
                        $logo = Image::cache(function ($image) {
                                return $image->make(urldecode("https://publicapis.lshtm.ac.uk/global/logo?width=170"));
                        }, 7200);
                        $variables = [
                            'template'         => $this->message->message,
                            'subject'          => $this->message->subject,
                            'logo'             => $logo,
                            'logo_name'        => "school_seal.png",
                            'application_name' => $this->message->template->application->title,
                            'signature'        => !empty($this->message->template->application->meta->signature) ? $this->message->template->application->meta->signature : config('app.name')
                        ];
                } catch (\Exception $e) {
                        $this->message->status = Message::STATUS_MESSAGE_ERROR;
                        $this->message->save();
                        Log::error(trans('app.messages.problem_sending_mail') . " because {$e->getMessage()} at line: {$e->getLine()}");
                        return false;
                }

                try {
                        $this->executeMailSend($mailer, $variables, $baseTemplate);
                } catch (\Exception  $e) {
                        $this->message->status = Message::STATUS_FAILED;
                        $this->message->save();
                        Log::error(trans('app.messages.problem_sending_mail') . " because {$e->getMessage()} at line: {$e->getLine()}");
                }
        }

        /**
         * @param $mailer Mailer
         * @param $variables
         * @param $baseTemplate
         */
        private function executeMailSend(Mailer $mailer, $variables, $baseTemplate)
        {
                $mailer->send('emails.' . $baseTemplate, $variables, function ($m) {
                        $sender = unserialize($this->message->from);
                        $m->from($sender["email"], $sender["name"]);
                        $m->to($this->message->to)->subject($this->message->subject);
                        if (count($this->message->attachments)) {
                                $attachments = $this->message->attachments;
                                $attachments->each(function ($attachment) use ($m) {
                                        /** @var Attachment $attachment */
                                        $m->attach($attachment->file_location);
                                });
                        }
                });
                $this->message->status = Message::STATUS_SENT;
                $this->message->save();
                Log::info(trans('app.messages.email_sent_successfully'));
        }

        public function failed(\Exception $e)
        {
                $this->message->status = Message::STATUS_FAILED;
                $this->message->save();
                Log::error(trans('app.messages.problem_sending_mail') . " because {$e->getMessage()} at line: {$e->getLine()}");
        }
}
