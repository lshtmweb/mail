<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 10/05/2016
 * Time: 16:57
 */

namespace App;

use Illuminate\Support\Facades\Log;

trait ParsesText
{

        /**
         * Parse selected text given certain parameters.
         * @param $params array An array of parameters.
         * @param $text string The string you want to parse.
         * @return string The parsed string.
         */
        public function parseText($params, $text, $end_delimiter)
        {
                $start_delimiter = config('app.start_delimeter');

                !empty($end_delimiter) ? $const_scheme = '/(\\' . $start_delimiter . ')((?:[a-z][a-z0-9_]*))' . $end_delimiter . '/is' : $const_scheme = '/(\\' . $start_delimiter . ')((?:[a-z][a-z0-9_]*))/is';
                preg_match_all($const_scheme, $text, $parameters);

                foreach ($parameters[0] as $parameter)
                {
                        $sparam = ltrim($parameter, $start_delimiter);
                        if (!empty($end_delimiter)) $sparam = rtrim($sparam, $end_delimiter);
                        if (array_has($params, $sparam))
                        {
                                $value = array_get($params, $sparam);
                                $text = str_replace($parameter, $value, $text, $replacement_count);

                                Log::info(trans('app.message_parameter_replacement_made'));
                        }
                        else
                        {
                                Log::debug(trans('app.message_parameter_not_found'));
                        }
                }
                return $text;
        }

        function explodeEscaped($delimiter, $str, $escapeChar, $start_delimiter, $end_delimiter)
        {
                //Just some random placeholders that won't ever appear in the source $str
                $double = "\0\0\0_doub";
                $escaped = "\0\0\0_esc";
                $str = str_replace($escapeChar . $escapeChar, $double, $str);

                !empty($end_delimiter) ? $str = str_replace($escapeChar . $end_delimiter, $escaped, $str) : $str = str_replace($escapeChar . $delimiter, $escaped, $str);

                $str = html_entity_decode($str);
                !empty($end_delimiter) ? $split = preg_split('/\\' . $start_delimiter . '*' . $end_delimiter . '/', $str) : $split = explode($delimiter, $str);

                foreach ($split as &$val)
                {
                        !empty($end_delimiter) ? $val = str_replace([$double, $escaped], [$escapeChar, $end_delimiter], $val) : $val = str_replace([$double, $escaped], [$escapeChar, $delimiter], $val);
                }

                return $split;
        }
}
