<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
class Controller extends BaseController
{
        protected function respond($data, $status = Response::HTTP_OK)
        {
                $r = $_SERVER['REQUEST_TIME_FLOAT'];
                $requestTime = microtime(true) - $r;
                $data = [
                        "results" => $data,
                        "meta"    => [
                                "count"        => sizeof((array)$data),
                                "about"        => config('app.name', 'Web Team Application'),
                                "request_time" => round($requestTime, 4, PHP_ROUND_HALF_UP)
                        ]
                ];
                return new JsonResponse($data, $status);
        }

        protected function error($data, $status = Response::HTTP_OK)
        {
                $r = $_SERVER['REQUEST_TIME_FLOAT'];
                $requestTime = microtime(true) - $r;
                $data = [
                        "errors" => $data,
                        "meta"    => [
                                "status"       => $status,
                                "about"        => config('app.name', 'Web Team Application'),
                                "request_time" => round($requestTime, 4, PHP_ROUND_HALF_UP)
                        ]
                ];
                return new JsonResponse($data, $status);
        }

}
