<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;

class MainController extends Controller
{
        /**
         * Create a new controller instance.
         * @return array
         */
        public function getVersion()
        {
                return $this->respond([
                    'London School of Hygiene and Tropical Medicine - Mail API',
                    'version' => config('app.version'),
                ]);
        }

        /**
         * @return array
         */
        public function helpInfo()
        {
                $routes = Cache::remember('routes', 120, function () {
                        return App::getRoutes();
                });
                return $this->respond([
                    'info'   => "This service exposes mail capabilities to applications. It also allows apps to send out SMS messages easily.",
                    'routes' => $routes
                ]);
        }
}
