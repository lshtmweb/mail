<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 03/05/2016
 * Time: 11:39
 */

namespace App\Http\Controllers;

use App\Application;
use App\Template;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class TemplateController extends Controller
{
        /**
         * @var Template
         */
        protected $template;

        public function __construct(Template $template)
        {
                $this->template = $template;
        }

        /**
         * Get all templates in the system.
         * @return array
         */
        public function index()
        {
                $templates = $this->template->all();
                return $this->respond($templates);
        }

        /**
         * @param $id
         * @return array
         */
        public function show($id)
        {
                $template = $this->template->where(['reference' => $id])->firstOrFail();
                $template->uses;
                $template->application->meta;
                return $this->respond($template);
        }

        /**
         * @param Request $request
         * @return array
         */
        public function store(Request $request)
        {
                $this->validate($request, [
                    'title' => 'required|string',
                    'content' => 'required',
                    'type' => 'required|integer',
                    'key' => 'required|exists:applications,key',
                    'template_key' => 'sometimes'
                ]);
                $template = $this->template;
                $template->title = $request->title;
                $template->content = $request->input('content');
                $template->type = $request->type;
                $template->base = Template::DEFAULT_BASE;
                $application = Application::where(['key' => $request->key])->first();
                $request->has('template_key') ? $template->key = trim(sha1($application->slug . "_" .base64_encode($request->template_key))) : false;
                try {
                        DB::transaction(function () use ($template, $application) {
                                $template->save();
                                $application->templates()->save($template);
                        });
                } catch (\Exception $e) {
                        abort(Response::HTTP_INTERNAL_SERVER_ERROR, $e->getMessage());
                }

                return $this->respond($template);
        }

        /**
         * @param Request $request
         * @param $id
         * @return array
         */
        public function update(Request $request, $id)
        {
                $template = $this->template->where('reference', $id)
                        ->orWhere('key', $id)->firstOrFail();

                $this->validate($request, [
                    'title' => 'string',
                    'content' => 'string',
                    'type' => 'integer'
                ]);
                if ($request->has('title')) {
                        $template->title = $request->title;
                }
                if ($request->has('content')) {
                        $template->content = $request->input('content');
                }
                if ($request->has('type')) {
                        $template->type = $request->type;
                }
                try {
                        DB::transaction(function () use ($template) {
                                $template->save();
                        });
                } catch (\Exception $e) {
                        abort(500);
                } finally {
                        return $this->respond([$template]);
                }
        }

        /**
         * @param $id
         * @return array
         */
        public function destroy($id)
        {
                $template = $this->template->where('reference', $id)->firstOrFail();
                try {
                        DB::transaction(function () use ($template) {
                                $template->delete();
                        });
                } catch (\Exception $e) {
                        abort(500);
                } finally {
                        return $this->respond([$template]);
                }
        }
}
