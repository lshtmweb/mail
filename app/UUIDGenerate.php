<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 10/05/2016
 * Time: 16:57
 */

namespace App;


use Faker\Provider\Uuid;

trait UUIDGenerate
{
        public static function bootUUIDGenerate()
        {
                static::creating(function ($model) {
                        $model->reference = (string)Uuid::uuid();
                });
        }

        public function getCasts()
        {
                return $this->casts;
        }


}