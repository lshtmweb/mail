<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 12/10/2016
 * Time: 11:14
 */

namespace App;


class Application extends BaseModel
{
        use GeneratesKeys, HasSlug;

        const STATUS_ACTIVE = 1;
        const STATUS_INACTIVE = 2;
        const STATUS_BLOCKED = 3;

        protected $fillable = ['title'];

        public function meta()
        {
                return $this->hasOne('App\ApplicationMeta', 'application_id');
        }

        public function messages()
        {
                return $this->hasManyThrough('App\Message', 'App\Template');
        }

        public function templates()
        {
                return $this->hasMany('App\Template', 'application_id');
        }

        public function getStoragePath()
        {
                return config('app.storage_path') . DIRECTORY_SEPARATOR . sha1(base64_encode($this->reference));
        }
}