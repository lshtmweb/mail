<?php
/**
 *
 * User: aitspeko
 * Date: 31/10/2018
 * Time: 16:25
 *
 * Project: mailer
 */

namespace App\Console\Commands;

use App\Message;
use Illuminate\Console\Command;
use Laravel\Lumen\Routing\DispatchesJobs;
use App\Application;
use Illuminate\Support\Facades\DB;
class NewApplicationCommand extends Command
{
        protected $signature = 'application:create {application_name}';
        protected $description = 'Register a new application with Mailer, returns the existing reference and key 
        if the application already exists. Usage:  php artisan application:create "New application name"';
        /**
         * @var Message
         */

        public function __construct()
        {
                parent::__construct();
        }

        public function handle()
        {
                $application = Application::firstOrNew(['title' => $this->argument('application_name')]);

                try
                {
                        DB::transaction(function () use ($application) {
                                $application->save();
                        });
                }
                catch (\Exception $e)
                {
                        $this->error($e->getMessage());
                }

                if ($application->wasRecentlyCreated === true)
                {
                        $this->info("New application created");
                }
                else
                {
                        $this->info("An application with that name already exists");
                }
                $this->info("Application reference: " . $application->reference);
                $this->info("Application key: " . $application->key);
        }
}