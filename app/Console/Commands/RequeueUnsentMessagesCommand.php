<?php
/**
 *
 * User: aitspeko
 * Date: 31/10/2018
 * Time: 16:25
 *
 * Project: mailer
 */

namespace App\Console\Commands;

use App\Jobs\SendEmail;
use App\Message;
use Carbon\Carbon;
use Illuminate\Console\Command;


class RequeueUnsentMessagesCommand extends Command
{
        protected $signature = 'messages:requeue --flush';
        /**
         * @var Message
         */
        private $message;

        public function __construct(Message $message)
        {
                parent::__construct();

                $this->message = $message;
        }

        public function handle()
        {
                $messages = $this->message->newQuery()
                    ->where('created_at', '>=', Carbon::now()->subDays(3)->toDateTimeString())
                    ->unsent()
                    ->get();
                $messages->each(function ($message) {
                        dispatch(new SendEmail($message));
                });
        }
}