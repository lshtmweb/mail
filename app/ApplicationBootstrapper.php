<?php
/**
 *
 * User: aitspeko
 * Date: 31/10/2018
 * Time: 17:44
 *
 * Project: mailer
 */

namespace App;


use Laravel\Lumen\Application as LumenApplication;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\RotatingFileHandler;

class ApplicationBootstrapper extends LumenApplication
{
        protected function getMonologHandler()
        {
                $maxRotatedFiles = 3;

                return (new RotatingFileHandler(storage_path('logs/lumen.log'), $maxRotatedFiles))
                    ->setFormatter(new LineFormatter(null, null, true, true));
        }
}