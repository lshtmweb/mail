<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 12/10/2016
 * Time: 11:57
 */

namespace App;


trait HasSlug
{
        public static function bootHasSlug()
        {
                static::creating(function ($model) {
                        $model->slug = strtolower(camel_case($model->title));
                });
        }
}