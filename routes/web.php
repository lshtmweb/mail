<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->get('/', function () use ($router) {
//    return $router->app->version();
//});


$router->get('/', 'MainController@getVersion');

$router->group(['prefix' => 'templates'], function ($app) {
        $app->get('/', 'TemplateController@index');
        $app->get('/{id}', 'TemplateController@show');
        $app->post('/', 'TemplateController@store');
        $app->put('/{id}', 'TemplateController@update');
        $app->delete('/{id}', 'TemplateController@destroy');
});

$router->group(['prefix' => 'applications'], function ($app) {
        //$app->get('/', 'TemplatesController@index');
        //$app->get('/{id}', 'TemplatesController@show');
        $app->post('/', 'ApplicationController@store');
});

$router->group(['prefix' => 'email'], function ($app) {
        $app->post('/', 'EmailController@store');
});