<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('applications', function (Blueprint $table) {
                        $table->increments('id');
                        $table->uuid('reference')->unique();
                        $table->string('title')->unique();
                        $table->string('slug')->unique();
                        $table->string('secret', 48);
                        $table->string('key', 48);
                        $table->tinyInteger('status')->default(\App\Application::STATUS_ACTIVE);
                        $table->timestamps();
                        $table->softDeletes();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('applications');
        }
}
