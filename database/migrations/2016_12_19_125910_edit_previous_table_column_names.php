<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EditPreviousTableColumnNames extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::table('messages', function (Blueprint $table) {
                        $table->renameColumn('template', 'template_id');
                });
                Schema::table('templates', function (Blueprint $table) {
                        $table->renameColumn('application', 'application_id');
                });
                Schema::table('messages', function (Blueprint $table) {
                        $table->unsignedInteger('template_id')->after('reference')->change();
                });
                Schema::table('templates', function (Blueprint $table) {
                        $table->unsignedInteger('application_id')->after('reference')->change();
                });

        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::table('messages', function (Blueprint $table) {
                        $table->renameColumn('template_id', 'template');
                });
                Schema::table('templates', function (Blueprint $table) {
                        $table->renameColumn('application_id', 'application');
                });
                Schema::table('messages', function (Blueprint $table) {
                        $table->integer('template')->after('reference')->change();
                });
                Schema::table('templates', function (Blueprint $table) {
                        $table->integer('application')->after('reference')->change();
                });
        }
}
