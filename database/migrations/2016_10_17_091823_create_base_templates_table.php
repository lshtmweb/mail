<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBaseTemplatesTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('base_templates', function (Blueprint $table) {
                        $table->increments('id');
                        $table->uuid('reference')->unique();
                        $table->string('name');
                        $table->string('slug');
                        $table->string('preview_image')->nullable();
                        $table->timestamps();
                        $table->softDeletes();
                });
    }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('base_templates');
        }
}
