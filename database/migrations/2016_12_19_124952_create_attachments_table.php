<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttachmentsTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('attachments', function (Blueprint $table) {
                        $table->increments('id');
                        $table->uuid('reference')->unique();
                        $table->unsignedInteger('message_id');
                        $table->string('file_name');
                        $table->string('file_location');
                        $table->timestamps();

                        $table->foreign('message_id')
                            ->references('id')
                            ->on('messages')
                            ->onUpdate('cascade')
                            ->onDelete('cascade');
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('attachments');
        }
}
