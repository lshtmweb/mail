<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationMetaTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('applications_meta', function (Blueprint $table) {
                        $table->increments('id');
                        $table->integer('application_id');
                        $table->string('from_name');
                        $table->string('from_email');
                        $table->text('signature');
                        $table->timestamps();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('applications_meta');
        }
}
