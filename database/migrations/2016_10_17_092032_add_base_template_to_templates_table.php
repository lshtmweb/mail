<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddBaseTemplateToTemplatesTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::table('templates', function (Blueprint $table) {
                        $table->integer('base')->after('reference')->nullable();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::table('templates', function (Blueprint $table) {
                        $table->dropColumn('base');
                });
        }
}
