<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
                Schema::create('templates', function (Blueprint $table) {
                        $table->increments('id');
                        $table->uuid('reference')->unique();
                        $table->string('title');
                        $table->smallInteger('type');
                        $table->text('content');
                        $table->integer('application');
                        $table->string('created_by');
                        $table->string('updated_by');
                        $table->timestamps();
                        $table->softDeletes();
                });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
                Schema::drop('templates');
        }
}
