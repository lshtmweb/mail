<?php

use Illuminate\Database\Seeder;

class ApplicationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $applications = collect([
            [
                'application' => ['reference' => '90ee3c0b-2316-3dde-8e4d-ea543b67afab',
                    'title' => 'paperless',
                    'slug' => 'paperless',
                    'secret' => 'afff37f34b9a4d1c93b7e27a6b6f9280e451a7f0',
                    'key' => '88baf8688787b552f31d8b9b070ef8e241d4537e',
                    'status' => 1],
                'application_meta' => [
                    'from_name' => 'LSHTM Admissions',
                    'from_email' => 'admissions@lshtm.ac.uk',
                    'signature' => '<p>Admissions Team</p>
<p>London School of Hygiene &amp; Tropical Medicine<br />Tel: +44 (0) 207299 4646<br />Email: admissions@lshtm.ac.uk</p>',

                ]
            ],
            [
                'application' => ['reference' => '0b2b27c-ec09-3527-b330-e5cf23e91177',
                    'title' => 'Scholarship',
                    'slug' => 'scholarship',
                    'secret' => 'cbc4ad5441f691c7ee9ed0cd6e7a0dffc2c2da11',
                    'key' => '6e2af755f6a72768379e10b64317a8360c506598',
                    'status' => 1],
                'application_meta' => [
                    'from_name' => 'LSHTM Scholarship',
                    'from_email' => 'noreply@lshtm.ac.uk',
                    'signature' => '',

                ]
            ],
            [
                'application' => ['reference' => 'eeaa3cdd-073b-3cb9-9f1f-c0622871d045',
                    'title' => 'Itravel',
                    'slug' => 'itravel',
                    'secret' => '9d22d8571152838fc1c4b131821f0d60a74b3522',
                    'key' => '623f4e7ebd73dbfa6f5472cb3a7fcd9666124ae6',
                    'status' => 1],
                'application_meta' => [
                    'from_name' => 'iTravel',
                    'from_email' => 'noreply@lshtm.ac.uk',
                    'signature' => '',
                ]
            ],
            [
                'application' => [
                    'reference' => '1eeacf2a-ec88-3d00-99cf-f8bc913145d6',
                    'title' => 'PFV',
                    'slug' => 'pfv',
                    'secret' => '380239ea0218e467bc8eba665b67fb6be13308a0',
                    'key' => '3feddb0ed4e9c711da758eeded42868cc5cb38b4',
                    'status' => 1],
                'application_meta' => [
                    'from_name' => 'Payroll Variations Funding',
                    'from_email' => 'noreply@lshtm.ac.uk',
                    'signature' => '',
                ]
            ],
            [
                'application' => ['reference' => '7a1a7b82-49f3-3482-b9b4-8da21bed4499',
                    'title' => 'TED Booking System',
                    'slug' => 'tedbookingsystem',
                    'secret' => '74bf375086edf45d510b6e03ed47444040d87d48',
                    'key' => '12e803b19928f906b0ccb2098d5291fc9a67c86f',
                    'status' => 1],
                'application_meta' => [
                    'from_name' => 'TED Booking',
                    'from_email' => 'noreply@lshtm.ac.uk',
                    'signature' => '',
                ]
            ],
            [
                'application' => ['reference' => '87063082-e40d-3885-b2ee-5d2b176721c7',
                    'title' => 'Fellowship',
                    'slug' => 'fellowship',
                    'secret' => '4d7803de7947b4a2f3aec42b058a417d29895968',
                    'key' => '200553ea6219e0e7b6a61efdb7431365b0151843',
                    'status' => 1],
                'application_meta' => [
                    'from_name' => 'LSHTM TEG Fellowship',
                    'from_email' => 'noreply@lshtm.ac.uk',
                    'signature' => '',
                ]
            ]

        ]);


        $applications->each(function ($application) {
            $application_id = DB::table('applications')->insertGetId($application['application']);
            $application_id = ['application_id' => $application_id];
            $application_meta_array = array_merge($application['application_meta'], $application_id);
            DB::table('applications_meta')->insert($application_meta_array);
        });
    }
}
