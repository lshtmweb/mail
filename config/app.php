<?php
/**
 * Created by PhpStorm.
 * User: aitspeko
 * Date: 25/01/2016
 * Time: 13:17
 */

return [

        /*
        |--------------------------------------------------------------------------
        | Encryption Key
        |--------------------------------------------------------------------------
        |
        | This key is used by the Illuminate encrypter service and should be set
        | to a random, 32 character string, otherwise these encrypted strings
        | will not be safe. Please do this before deploying an application!
        |
        */

    'key' => env('APP_KEY', 'SomeRandomString!!!'),
    'log' => 'daily',

    'cipher' => 'AES-256-CBC',

        /*
        |--------------------------------------------------------------------------
        | Application Locale Configuration
        |--------------------------------------------------------------------------
        |
        | The application locale determines the default locale that will be used
        | by the translation service provider. You are free to set this value
        | to any of the locales which will be supported by the application.
        |
        */

    'locale' => env('APP_LOCALE', 'en'),

        /*
        |--------------------------------------------------------------------------
        | Application Fallback Locale
        |--------------------------------------------------------------------------
        |
        | The fallback locale determines the locale to use when the current one
        | is not available. You may change the value to correspond to any of
        | the language folders that are provided through your application.
        |
        */

    'fallback_locale' => env('APP_FALLBACK_LOCALE', 'en'),

    'version'         => 0.2,
    'mail'            => 'webmaster@lshtm.ac.uk',
    'name'            => 'LSHTM Mailer Service',
    'start_delimeter' => "$",
    'end_delimeter'   => "",
    'delimeter'       => ";",
    'escape_char'     => '\\',
    'storage_path'    => env('STORAGE_PATH', storage_path('app/files/')),
    'support_mime_type' => env('SUPPORT_MIME_TYPE', "jpeg,png,pdf,doc,docx,zip,ics")
];
