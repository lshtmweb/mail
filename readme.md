## LSHTM Mail Framework

[![Build Status](https://scrutinizer-ci.com/b/lshtmweb/mail/badges/build.png?b=master&s=f26a9de7ca967befa59d66330c6b2328fc8c9cbd)](https://scrutinizer-ci.com/b/lshtmweb/mail/build-status/master)
[![Code Coverage](https://scrutinizer-ci.com/b/lshtmweb/mail/badges/coverage.png?b=master&s=26bed08eed076bf6a586bf6d9e37178858935edd)](https://scrutinizer-ci.com/b/lshtmweb/mail/?branch=master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/lshtmweb/mail/badges/quality-score.png?b=master&s=f4b8a52c2d1e1e1eb40b8cec4edd4c372c1f3aad)](https://scrutinizer-ci.com/b/lshtmweb/mail/?branch=master)

This component allows applications to send email via a simple API command. It works with the authentication server to confirm an application is able to carry out the proposed task.

This is part of the LSHTMWeb OpenSource Programme

## Security Vulnerabilities

If you discover a security vulnerability within this application, please send an e-mail to the web team at webeditor@lshtm.ac.uk. All security vulnerabilities will be promptly addressed.

### License

The license for this application is still pending

## Docker setup - for local development using docker-compose
Require Docker to be installed: https://www.docker.com/products/docker-desktop
Edit your PC hosts file and add your local domain mapping

Next to localhost add the following for the IP 127.0.0.1  localhost  mail.test db.mail.test 

Once docker installed:  
copy .env-example to .env and setup initial variables required by the application 
(including database host,db user and password) - these values will be used to create the intial database container host, username, password in docker.

From command line from within the project directory start the local build
run the following command: 

    docker-compose up -d --build

This should build docker containers

Review docker-compose for ports to access web interface and mysql database

    docker-compose exec web composer install  
    docker-compose exec web php artisan migrate
    docker-compose exec web php artisan db:seed
     
   You can run other commands as needed using the same concept above
     
   You should now be able to visit the system's base url, such as "mail.test:8002"

## Registering a new application with Mailer

Run the following command to register a new application with Mailer, replace the name within the double quotes with your application's name

    php artisan application:create "new application name"

Docker example of the above command:

    docker-compose exec web php artisan application:create "Biobank"
    
## Alternative way to create a new application

Create a POST request to the /applications endpoint. In the body of the POST request, specify a "name" key where the value is the name of the application to be created
